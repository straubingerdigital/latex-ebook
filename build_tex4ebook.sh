#!/usr/bin/sh

# make the first dependency
wget https://github.com/michal-h21/make4ht/archive/v0.1.zip
unzip v0.1.zip
rm -rf v0.1.zip
cd make4ht-0.1
make
make install
cd .. && rm -rf make4ht-0.1

# make tex4ebook
wget https://github.com/michal-h21/tex4ebook/archive/v0.1.zip
unzip v0.1.zip
rm -rf v0.1.zip
cd tex4ebook-0.1
make
make install
cd .. && rm -rf tex4ebook-0.1
