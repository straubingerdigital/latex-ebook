FROM debian:jessie
MAINTAINER Jorge Araya Navarro <elcorreo@deshackra.com>

RUN apt-get update --fix-missing -qq -y \
  && apt-get install -y \
    pandoc \
    texlive-binaries
    latexmk \
    tidy \
    curl \
    zip \
    wget \
    uuid-runtime \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

ADD build_tex4ebook.sh /root/
RUN /root/build_tex4ebook.sh

ADD process.bash /root/
RUN chmod +x /root/process.bash

ENTRYPOINT /root/process.bash -
