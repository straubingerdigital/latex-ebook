[![](https://images.microbadger.com/badges/image/shackra/latex-ebook.svg)](https://microbadger.com/images/shackra/latex-ebook "Get your own image badge on microbadger.com")

This docker image can be use to make PDF files and also epub, epub 3 and mobi files. shamelessly based on https://hub.docker.com/r/narf/latex/

## Usage

    docker pull shackra/latex-ebook

    docker run -i shackra/latex-ebook < ~/your\ book.latex > files.zip
