#!/usr/bin/env bash

cd /tmp

jobname=`uuidgen`

cat /dev/fd/0>/tmp/$jobname.latex

xelatex --interaction=nonstopmode /tmp/$jobname.latex &>/dev/null
# Twice, in case of TOC and stuff
xelatex --interaction=nonstopmode /tmp/$jobname.latex &>/dev/null
# make epub
tex4ebook --tidy /tmp/$jobname.latex &>/dev/null
# # epub3
# tex4ebook --tidy --format epub3 /tmp/$jobname.latex &>/dev/null
tex4ebook --tidy --format mobi /tmp/$jobname.latex &>/dev/null
# compress the results
zip latex-results.zip $jobname.epub $jobname.pdf $jobname.mobi
cat /tmp/latex-results.zip
